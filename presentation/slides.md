---
theme: 'default'
footer: '*Slides and demos available at <https://gitlab.com/gabmus/python-hypothesis-presentation>*<br/><br/>2020 © Gabriele Musco — This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/) <br /> http://creativecommons.org/licenses/by-sa/4.0/'
style: |
    h1 {
        color: #2ec27e;
    }
    footer {
        display: none;
        text-align: center;
        width: 100%;
    }
---

<style scoped>
footer {
    display: block;
}
</style>

# Hypothesis

## Awesome PBT framework for Python

##### Presented by Gabriele Musco

---

# ![w:256](https://hypothesis.readthedocs.io/en/latest/_images/dragonfly-rainbow.svg) What is *Hypothesis*?

> Hypothesis is a Python library for creating unit tests which are simpler to write and more powerful when run [...].
>
> It works by letting you write tests that assert that something should be true for every case, not just the ones you happen to think of.

In a nutshell, Hypothesis is a PBT framework for Python.

---

# What is *Hypothesis*?

![w:128](https://avatars2.githubusercontent.com/u/28623?s=460&v=4)

Created by mathematician and software developer David R. MacIver.

Hypothesis' original goal is to be an implementation of Quickcheck, the first PBT framework ever created, written in 1999 for Haskell.

The current mainstream version of Hypothes is the one for the Python language, but the long term plan is to make it language agnostic. 

---

# What is *Hypothesis*?

But the project has even higher ambitions:

> The goal of Hypothesis is to bring advanced testing techniques to the masses, and to provide an implementation that is so high quality that it is easier to use them than it is not to use them. Where I can, I will beg, borrow and steal every good idea I can find that someone has had to make software testing better. Where I can’t, I will invent new ones.
>
> Quickcheck is the start, but I also plan to integrate ideas from fuzz testing [...], and am open to and actively seeking out other suggestions and ideas.

---

# The purpose of Hypothesis

> Software is, as they say, eating the world. Software is also terrible. It's buggy, insecure and generally poorly thought out.

The purpose of Hypothesis is to make property based testing the norm. If for any reason you cannot integrate Hypothesis in your testing workflow, that's a bug in your program.

In a way, forcing developers to use Hypothesis, or PBT in general for that matter, is a great way to make sure that the program is well (or at least somewhat better) written, and has a simple and most importantly usable API.

---

# A very simple example

```python
# test_sum.py
from hypothesis import given
from hypothesis.strategies import integers


def sum(x, y):
    return x+y


@given(integers(), integers())
def test_sum(x, y):
    assert sum(x, y) == x + y
```

```bash
python3 -m pytest --hypothesis-show-statistics 
```

---

# Output

```text
========================== test session starts ==========================
platform linux -- Python 3.8.3, pytest-5.4.3, py-1.8.1, pluggy-0.13.1
rootdir: /tmp/hypotesis_dumb
plugins: cov-2.9.0, hypothesis-5.16.1
collected 1 item

test_sum.py .                                                     [100%]
========================= Hypothesis Statistics =========================

test_sum.py::test_sum:

  - during generate phase (0.05 seconds):
    - Typical runtimes: < 1ms, ~ 55% in data generation
    - 100 passing examples, 0 failing examples, 0 invalid examples

  - Stopped because settings.max_examples=100


=========================== 1 passed in 0.07s ===========================

```

---

# A very simple example

- `@given()` allows you to pass different data types to the test, using **strategies** that will then be passed in the test function as its arguments
- `@integers()` is a very simple strategy that draws integers
- we use the standard `assert` mechanism built into Python
- we use **pytest** module to run the hypothesis tests
  - pytest has an automatic test discovery system, it looks for tests in files matching `test_*.py` or `*_test.py` and selects functions that are `test` prefixed
  - the optional `--hypothesis-show-statistics` option gives us statistics on the tests that have been ran

---

# Complicating it a bit

```python
# test_division.py
from hypothesis import given
from hypothesis.strategies import integers


def division(x, y):
    return x/y


@given(integers(), integers())
def test_division(x, y):
    z = x*y
    assert division(z, y) == x
```

---

# Of course it fails

```text
========================== test session starts ==========================
platform linux -- Python 3.8.3, pytest-5.4.3, py-1.8.1, pluggy-0.13.1
rootdir: /tmp/hypotesis_dumb
plugins: cov-2.9.0, hypothesis-5.16.1
collected 1 item

test_division.py F                                                [100%]

=============================== FAILURES ================================
_____________________________ test_division _____________________________

    @given(integers(), integers())
>   def test_division(x, y):
E   hypothesis.errors.MultipleFailures: Hypothesis found 2 distinct failures.

test_division.py:11: MultipleFailures
------------------------------ Hypothesis -------------------------------
Falsifying example: test_division(
    x=9007203566551297, y=1,
)
Traceback (most recent call last):
  File "/tmp/hypotesis_dumb/test_division.py", line 13, in test_division
    assert division(z, y) == x
AssertionError: assert 9007203566551296.0 == 9007203566551297
 +  where 9007203566551296.0 = division(9007203566551297, 1)

Falsifying example: test_division(
    x=0, y=0,
)
Traceback (most recent call last):
  File "/tmp/hypotesis_dumb/test_division.py", line 13, in test_division
    assert division(z, y) == x
  File "/tmp/hypotesis_dumb/test_division.py", line 7, in division
    return x/y
ZeroDivisionError: division by zero
======================== short test summary info ========================
FAILED test_division.py::test_division - hypothesis.errors.MultipleFai...
=========================== 1 failed in 0.24s ===========================
```

---

# Let's add some restrictions

We want `y` to not be `0`. We can do it a couple different ways:

- filtering our strategy
- making an assumption

---

# Filtering

```python
# test_division.py
from hypothesis import given
from hypothesis.strategies import integers


def division(x, y):
    return x/y


@given(integers(), integers().filter(lambda a: a != 0))
def test_division(x, y):
    z = x*y
    assert division(z, y) == x
```

---

# And now... We found an error?

```text
========================== test session starts ==========================
platform linux -- Python 3.8.3, pytest-5.4.3, py-1.8.1, pluggy-0.13.1
rootdir: /tmp/hypotesis_dumb
plugins: cov-2.9.0, hypothesis-5.16.1
collected 1 item

test_division.py F                                                [100%]

=============================== FAILURES ================================
_____________________________ test_division _____________________________

    @given(integers(), integers().filter(lambda a: a != 0))
>   def test_division(x, y):

test_division.py:11:
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

x = 36028797018996993, y = 1

    @given(integers(), integers().filter(lambda a: a != 0))
    def test_division(x, y):
        z = x*y
>       assert division(z, y) == x
E       assert 3.602879701899699e+16 == 36028797018996993
E        +  where 3.602879701899699e+16 = division(36028797018996993, 1)

test_division.py:13: AssertionError
------------------------------ Hypothesis -------------------------------
Falsifying example: test_division(
    x=36028797018996993, y=1,
)
======================== short test summary info ========================
FAILED test_division.py::test_division - assert 3.602879701899699e+16 ...
=========================== 1 failed in 0.27s ===========================
```

---

Turns out that when you do divisions on large integers in Python you lose precision, so we've got to use integer division with the `//` operator.

---

# Fixing the error

```python
# test_division.py
from hypothesis import given
from hypothesis.strategies import integers


def division(x, y):
    return x//y  # <<<<<


@given(integers(), integers().filter(lambda a: a != 0))
def test_division(x, y):
    z = x*y
    assert division(z, y) == x
```

---

# And now everything's working as expected

```text
========================== test session starts ==========================
platform linux -- Python 3.8.3, pytest-5.4.3, py-1.8.1, pluggy-0.13.1
rootdir: /tmp/hypotesis_dumb
plugins: cov-2.9.0, hypothesis-5.16.1
collected 1 item

test_division.py .                                                [100%]

=========================== 1 passed in 0.08s ===========================
```

---

# Assumptions

```python
# test_division.py
from hypothesis import given, assume  # <<<<<
from hypothesis.strategies import integers


def division(x, y):
    return x//y


@given(integers(), integers())
def test_division(x, y):
    assume(y != 0)  # <<<<<
    z = x*y
    assert division(z, y) == x
```

---

# Some *settings*

It's possible to customize how certain tests are run using the `@settings` decorator.

The most notable ones available are:

- `max_examples`
- `deadline`

<!--<https://hypothesis.readthedocs.io/en/latest/settings.html> -->

---

# Max Examples

Raises or lowers the number of examples drawn for a certain test. The default value is 100

```python
from hypothesis import given, settings
from hypothesis.strategies import integers


@given(integers(), integers())
@settings(max_examples=500)
def test_division(x, y):
    assume(y != 0)  # <<<<<
    z = x*y
    assert division(z, y) == x
```

---

# Deadline

Specifies the maximum duration (as either a number representing milliseconds or a `timedelta` object) a certain test is allowed to run.

**This is not a timeout!** The test will still run if it exceeds the deadline, but it will be reported as failed. The default value is 200 milliseconds, so you may need to tweak this setting if you expect your test to take more than that.

```python
from hypothesis import given, settings
from hypothesis.strategies import integers
from datetime import timedelta
from time import sleep


@given(integers())
@settings(deadline=timedelta(milliseconds=50), max_examples=1)
def test_sleep(x):
    sleep(1)  # sleeps for 1 second
    assert x == x
```

---

# Output

```text
========================== test session starts ==========================
platform linux -- Python 3.8.3, pytest-5.4.3, py-1.8.1, pluggy-0.13.1
rootdir: /home/gabmus/git/hypothesis-presentation/demo
plugins: hypothesis-5.16.1, cov-2.10.0
collected 1 item

hypothesis/test_deadline.py F                                     [100%]

=============================== FAILURES ================================
______________________________ test_sleep _______________________________

    @given(integers())
>   @settings(deadline=timedelta(milliseconds=50), max_examples=1)

hypothesis/test_deadline.py:8:
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
/usr/lib/python3.8/site-packages/hypothesis/core.py:611: in execute_once
    result = self.test_runner(data, run)
/usr/lib/python3.8/site-packages/hypothesis/executors.py:52: in default_new_style_executor
    return function(data)
/usr/lib/python3.8/site-packages/hypothesis/core.py:607: in run
    return test(*args, **kwargs)
hypothesis/test_deadline.py:8: in test_sleep
    @settings(deadline=timedelta(milliseconds=50), max_examples=1)
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

args = (0,), kwargs = {}, initial_draws = 1, start = 9128.083365623
result = None, finish = 9129.083964703, internal_draw_time = 0
runtime = datetime.timedelta(seconds=1, microseconds=599)
current_deadline = timedelta(milliseconds=50)

    @proxies(self.test)
    def test(*args, **kwargs):
        self.__test_runtime = None
        initial_draws = len(data.draw_times)
        start = perf_counter()
        result = self.test(*args, **kwargs)
        finish = perf_counter()
        internal_draw_time = sum(data.draw_times[initial_draws:])
        runtime = datetime.timedelta(
            seconds=finish - start - internal_draw_time
        )
        self.__test_runtime = runtime
        current_deadline = self.settings.deadline
        if not is_final:
            current_deadline = (current_deadline // 4) * 5
        if runtime >= current_deadline:
>           raise DeadlineExceeded(runtime, self.settings.deadline)
E           hypothesis.errors.DeadlineExceeded: Test took 1000.60ms, which exceeds the deadline of 50.00ms

/usr/lib/python3.8/site-packages/hypothesis/core.py:542: DeadlineExceeded
------------------------------ Hypothesis -------------------------------
Falsifying example: test_sleep(
    x=0,
)
========================= Hypothesis Statistics =========================
hypothesis/test_deadline.py::test_sleep:

  - during reuse phase (1.00 seconds):
    - Typical runtimes: ~ 1001ms, ~ 0% in data generation
    - 0 passing examples, 1 failing examples, 0 invalid examples
    - Found 1 failing example in this phase

  - during shrink phase (1.00 seconds):
    - Typical runtimes: ~ 1001ms, ~ 0% in data generation
    - 0 passing examples, 1 failing examples, 0 invalid examples
    - Tried 1 shrinks of which 0 were successful

  - Stopped because nothing left to do


======================== short test summary info ========================
FAILED hypothesis/test_deadline.py::test_sleep - hypothesis.errors.Dea...
=========================== 1 failed in 3.18s ===========================

```

---

# Back to the Triangle

Let's examine the seemingly trivial but actually quite interesting Triangle problem:

```python
from typing import Tuple
from functools import reduce
from operator import and_, or_


class Triangle:
    def __init__(self, sides: Tuple[int, int, int]):
        assert reduce(and_, [side > 0 for side in sides]), \
                'Sides must be positive'
        assert reduce(and_, [
            sides[-i] < (sides[1-i] + sides[2-i]) for i in range(3)
        ]), 'One side cannot be bigger than the sum of the other two'
        self.sides = sides

    def describe(self) -> str:
        return 'equilateral' if reduce(
            and_,
            [self.sides[1-i] == self.sides[-i] for i in range(2)]
        ) else (
            'isosceles' if reduce(
                or_,
                [self.sides[-i] == self.sides[1-i] for i in range(3)]
            ) else 'scalene'
        )
```

---

This particular example is interesting because the code is very concise and somewhat elegant compared to a more straight forward but verbose procedural implementation.

Of course, this conciseness could hide a bug.

---

# First, let's test equilateral triangles

This example is rather straight forward:

```python
from src.triangle import Triangle
from hypothesis import given
from hypothesis import strategies as st


@given(st.integers(min_value=1))
def test_equilateral_triangles(x):
    assert Triangle((x, x, x)).describe() == 'equilateral'
```

The `min_value` argument in the `integers` strategy allows us to set a constraint, as otherwise we would get examples `<= 0` that would lead to errors.

---

# What about any triangle?

```python
@given(
        st.tuples(
            st.integers(min_value=1),
            st.integers(min_value=1),
            st.integers(min_value=1)
        ).filter(
            lambda t: t[0] < t[1] + t[2] and
            t[1] < t[0] + t[2] and
            t[2] < t[0] + t[1]
        )
)
def test_valid_triangles(triangle_tuple):
    assert Triangle(triangle_tuple).describe() in (
        'scalene', 'isosceles', 'equilateral'
    )
```

We used the `tuples` strategy, that takes other strategies as arguments to draw a tuple of our design.

We also use `filter` here to make sure our triangle sides are valid.

---

# Too many values have been discarded

... and a similar thing happens with `assume`

```python
========================== test session starts ==========================
platform linux -- Python 3.8.3, pytest-5.4.3, py-1.8.1, pluggy-0.13.1
rootdir: /home/gabmus/git/hypothesis-presentation/demo
plugins: cov-2.9.0, hypothesis-5.16.1
collected 1 item

hypothesis/test_any_triangle_filter.py .                          [100%]
========================= Hypothesis Statistics =========================

hypothesis/test_any_triangle_filter.py::test_valid_triangles:

  - during reuse phase (0.00 seconds):
    - Typical runtimes: < 1ms, ~ 55% in data generation
    - 1 passing examples, 0 failing examples, 0 invalid examples

  - during generate phase (0.19 seconds):
    - Typical runtimes: < 1ms, ~ 80% in data generation
    - 99 passing examples, 0 failing examples, 128 invalid examples
    - Events:
      * 72.69%, Retried draw from tuples(integers(min_value=1), integers(min_value=1),
        integers(min_value=1)).filter(lambda t: t[0] < t[1] + t[2]) to satisfy filter
      * 56.39%, Aborted test because unable to satisfy tuples(
            integers(min_value=1), integers(min_value=1), integers(min_value=1)
        ).filter(lambda t: t[0] < t[1] + t[2])

  - Stopped because settings.max_examples=100


=========================== 1 passed in 0.21s ===========================
```

---

# A better solution: composite strategies

```python
@st.composite
def triangle_valid_integers_tuple(draw):
    x = draw(st.integers(min_value=1))
    y = draw(st.integers(min_value=1))
    z = draw(st.integers(min_value=abs(x-y)+1, max_value=(x + y - 1)))
    return (x, y, z)


@given(triangle_valid_integers_tuple())
def test_valid_triangles_with_custom_strategy(triangle_tuple):
    assert Triangle(triangle_tuple).describe() in (
        'scalene', 'isosceles', 'equilateral'
    )
```

With the `composite` decorator we can define a new strategy, composed from other strategies.

`draw` is a function we can call to get an example the same way it would happen when using the `@given` decorator in our test.

---

# More about Triangle

- for testing isosceles triangles we can either use another composite strategy similar to the previous one where we simply assign `y = x` or either `filter` or `assume` on the composite strategy itself as this time the discarded values won't be too many.
- for scalenes the easiest approach is to still use the previous composite strategy with `filter` or `assume`.

---

# Stateful and Model Based testing with State Machines

---

# When to use stateful testing

In a traditional testing scenario, even in PBT, tests are comprised by the sequence of actions to be executed and the expected result(s) of these actions.

Stateful testing takes this approach to another level: *any* valid sequence of actions should keep the state consistent and predictable.

If a particular sequence of actions, that should be valid on paper, makes the state inconsistent or different from what we expect, then there's a bug.

---

# It doesn't need to be stateful

As the documentation of Hypothesis says:

> Both of these names *(stateful testing, model based testing, Ed.)* are somewhat misleading: You don’t really need any sort of formal model of your code to use this, and it can be just as useful for pure APIs that don’t involve any state as it is for stateful ones.

---

# The *Counter* example

Very simple example of a Counter, an object with a state:

```python
class Counter:
    def __init__(self, start=0):
        self.n = start
        self.count = 0
    
    def dec(self):
        self.n -= 1
        self.count += 1

    def inc(self):
        self.n += 1
        self.count += 1

    def get(self):
        return self.n

    def reset(self):
        self.n = 0

    def double_it(self):
        self.n += self.n
```

---

# State Machine (in its simplest form)

```python
from src.counter import Counter
from hypothesis.stateful import rule, precondition, RuleBasedStateMachine


class CounterMachine(RuleBasedStateMachine):
    def __init__(self):
        super(CounterMachine, self).__init__()
        self.counter = Counter()

    @rule()
    @precondition(lambda self: self.counter.get() >= 0)
    def decrement(self):
        old = self.counter.get()
        self.counter.dec()
        assert self.counter.get() == old - 1

    @rule()
    def increment(self):
        old = self.counter.get()
        self.counter.inc()
        assert self.counter.get() == old + 1

    @rule()
    def reset_counter(self):
        self.counter.reset()
        assert self.counter.get() == 0

    @rule()
    def double_value(self):
        old = self.counter.get()
        self.counter.double_it()
        assert self.counter.get() == old*2


TestCounterMachine = CounterMachine.TestCase
```

---

- First we define a new class extending the `RuleBasedStateMachine` provided in `hypothesis.stateful`
- in its constructor we create our SUT object
- then we define a series of **rules**, which is the hypothesis lingo for the actions that we can run on the SUT
- rules can have **preconditions**
- the last line `TestCounterMachine = CounterMachine.TestCase` allows our test to be discoverable by pytest

---

# Output

```text
========================== test session starts ==========================
platform linux -- Python 3.8.3, pytest-5.4.3, py-1.8.1, pluggy-0.13.1
rootdir: /home/gabmus/git/hypothesis-presentation/demo
plugins: cov-2.9.0, hypothesis-5.16.1
collected 1 item

hypothesis/counter_test.py .                                      [100%]
========================= Hypothesis Statistics =========================

hypothesis/counter_test.py::TestCounterMachine::runTest:

  - during reuse phase (0.00 seconds):
    - Typical runtimes: < 1ms, ~ 14% in data generation
    - 1 passing examples, 0 failing examples, 0 invalid examples

  - during generate phase (1.58 seconds):
    - Typical runtimes: 0-9 ms, ~ 4% in data generation
    - 99 passing examples, 0 failing examples, 43 invalid examples
    - Events:
      * 88.03%, Retried draw from sampled_from([Rule(targets=(), function
=CounterMachine.decrement, arguments={}, precondition=<function CounterMa
chine.<lambda> at 0x7fcbf0163940>, bundles=()),
 Rule(targets=(), function=CounterMachine.double_value, arguments={}, pre
condition=None, bundles=()),
 Rule(targets=(), function=CounterMachine.increment, arguments={}, precon
dition=None, bundles=()),
 Rule(targets=(), function=CounterMachine.reset_counter, arguments={}, pr
econdition=None, bundles=())]).filter(RuleStrategy(machine=CounterMachine
({...})).is_valid).filter(lambda r: <unknown>) to satisfy filter
      * 23.94%, Aborted test because unable to satisfy sampled_from([Rule
(targets=(), function=CounterMachine.decrement, arguments={}, preconditio
n=<function CounterMachine.<lambda> at 0x7fcbf0163940>, bundles=()),
 Rule(targets=(), function=CounterMachine.double_value, arguments={}, pre
condition=None, bundles=()),
 Rule(targets=(), function=CounterMachine.increment, arguments={}, precon
dition=None, bundles=()),
 Rule(targets=(), function=CounterMachine.reset_counter, arguments={}, pr
econdition=None, bundles=())]).filter(RuleStrategy(machine=CounterMachine
({...})).is_valid).filter(lambda r: <unknown>)

  - Stopped because settings.max_examples=100


=========================== 1 passed in 1.61s ===========================
```

---

# Let's see what happens if we introduce a bug

```python
class BuggyCounter:
    def __init__(self, start=0):
        self.n = start
        self.count = 0
    
    def dec(self):
        self.n -= 1
        self.count += 1

    def inc(self):
        if self.count != 5:  # this is the bug
            self.n += 1
        self.count += 1

    def get(self):
        return self.n

    def reset(self):
        self.n = 0

    def double_it(self):
        self.n += self.n
```

---

# Output

*(Some parts have been omitted to fit the slide)*

```text
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

self = CounterMachine({})

    @rule()
    def increment(self):
        old = self.counter.get()
        self.counter.inc()
>       assert self.counter.get() == old + 1
E       AssertionError: assert 1 == (1 + 1)
E        +  where 1 = <bound method BuggyCounter.get of <src.counter.Bugg
yCounter object at 0x7f799cb6a370>>()
E        +    where <bound method BuggyCounter.get of <src.counter.BuggyC
ounter object at 0x7f799cb6a370>> = <src.counter.BuggyCounter object at 0
x7f799cb6a370>.get
E        +      where <src.counter.BuggyCounter object at 0x7f799cb6a370>
 = CounterMachine({}).counter

hypothesis/buggy_counter_test.py:21: AssertionError
------------------------------ Hypothesis -------------------------------
Falsifying example:
state = CounterMachine()
state.increment()
state.decrement()
state.increment()
state.decrement()
state.increment()
state.increment()
state.teardown()
```

---

# Reproducing the bug is easy!

Hypothesis gives us a shrinked sequence of operations to reproduce the bug.

```text
Falsifying example:
state = CounterMachine()
state.increment()
state.decrement()
state.increment()
state.decrement()
state.increment()
state.increment()
state.teardown()
```

---

# Using a model

```python
from src.counter import BuggyCounter
from hypothesis.stateful import (
    rule,
    precondition,
    invariant,
    RuleBasedStateMachine
)


class CounterMachine(RuleBasedStateMachine):
    def __init__(self):
        super(CounterMachine, self).__init__()
        self.counter = BuggyCounter()
        self.model = 0

    @invariant()
    def model_comparison(self):
        assert self.counter.get() == self.model

    @rule()
    @precondition(lambda self: self.counter.get() > 0)
    def decrement(self):
        self.counter.dec()
        self.model -= 1

    @rule()
    def increment(self):
        self.counter.inc()
        self.model += 1

    @rule()
    def reset_counter(self):
        self.counter.reset()
        self.model = 0

    @rule()
    def double_value(self):
        self.counter.double_it()
        self.model += self.model


TestCounterMachine = CounterMachine.TestCase
```

---

- We define a model that should behave like our SUT, it can be a simplified or less efficient version of it
- we can use **invariants** to make our life easier and avoid redundant assertions
- it behaves exactly like the previous example

---

# Thank you

## More about Hypothesis

- Home page: <https://hypothesis.works/>
- Documentation: <https://hypothesis.readthedocs.io>
- Repository: <https://github.com/HypothesisWorks/hypothesis/>
