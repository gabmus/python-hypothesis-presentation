production code in `src`, pbt tests in `hypotesis`

running tests like this

```bash
python -m pytest --hypothesis-show-statistics hypothesis/mytest.py
```
