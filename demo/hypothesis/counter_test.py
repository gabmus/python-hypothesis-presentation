from src.counter import Counter
from hypothesis.stateful import rule, precondition, RuleBasedStateMachine


class CounterMachine(RuleBasedStateMachine):
    def __init__(self):
        super(CounterMachine, self).__init__()
        self.counter = Counter()

    @rule()
    @precondition(lambda self: self.counter.get() >= 0)
    def decrement(self):
        old = self.counter.get()
        self.counter.dec()
        assert self.counter.get() == old - 1

    @rule()
    def increment(self):
        old = self.counter.get()
        self.counter.inc()
        assert self.counter.get() == old + 1

    @rule()
    def reset_counter(self):
        self.counter.reset()
        assert self.counter.get() == 0

    @rule()
    def double_value(self):
        old = self.counter.get()
        self.counter.double_it()
        assert self.counter.get() == old*2


TestCounterMachine = CounterMachine.TestCase
