from src.counter import BuggyCounter
from hypothesis.stateful import (
    rule,
    precondition,
    invariant,
    RuleBasedStateMachine
)


class CounterMachine(RuleBasedStateMachine):
    def __init__(self):
        super(CounterMachine, self).__init__()
        self.counter = BuggyCounter()
        self.model = 0

    @invariant()
    def model_comparison(self):
        assert self.counter.get() == self.model

    @rule()
    @precondition(lambda self: self.counter.get() > 0)
    def decrement(self):
        self.counter.dec()
        self.model -= 1

    @rule()
    def increment(self):
        self.counter.inc()
        self.model += 1

    @rule()
    def reset_counter(self):
        self.counter.reset()
        self.model = 0

    @rule()
    def double_value(self):
        self.counter.double_it()
        self.model += self.model


TestCounterMachine = CounterMachine.TestCase
