from src.triangle import Triangle
from hypothesis import given
from hypothesis import strategies as st


@given(
        st.tuples(
            st.integers(min_value=1),
            st.integers(min_value=1),
            st.integers(min_value=1)
        ).filter(
            lambda t: t[0] < t[1] + t[2] and
            t[1] < t[0] + t[2] and
            t[2] < t[0] + t[1]
        )
)
def test_valid_triangles(triangle_tuple):
    assert Triangle(triangle_tuple).describe() in (
        'scalene', 'isosceles', 'equilateral'
    )
