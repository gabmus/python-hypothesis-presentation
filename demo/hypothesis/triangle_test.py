from src.triangle import Triangle
from hypothesis import given, assume
from hypothesis import strategies as st


# @given(st.integers(), st.integers(), st.integers())
# def test_any_triangle(x, y, z):
#     assert Triangle((x, y, z)).describe() in (
#         'scalene', 'isosceles', 'equilateral'
#     )
# 
# 
# def filter_positive(x):
#     return x > 0
# 
# 
# @given(
#         st.integers().filter(filter_positive),
#         st.integers().filter(filter_positive),
#         st.integers().filter(filter_positive)
# )
# def test_any_triangle_filter(x, y, z):
#     assert Triangle((x, y, z)).describe() in (
#         'scalene', 'isosceles', 'equilateral'
#     )
# 
# 
# @given(st.integers(), st.integers(), st.integers())
# def test_any_triangle_assume(x, y, z):
#     assume(x > 0 and y > 0 and z > 0)
#     assert Triangle((x, y, z)).describe() in (
#         'scalene', 'isosceles', 'equilateral'
#     )


@given(st.integers(min_value=1))
def test_equilateral_triangles(x):
    assert Triangle((x, x, x)).describe() == 'equilateral'


@given(
        st.tuples(
            st.integers(min_value=1),
            st.integers(min_value=1),
            st.integers(min_value=1)
        ).filter(
            lambda t: t[0] < t[1] + t[2] and
            t[1] < t[0] + t[2] and
            t[2] < t[0] + t[1]
        )
)
def test_valid_triangles(triangle_tuple):
    assert Triangle(triangle_tuple).describe() in (
        'scalene', 'isosceles', 'equilateral'
    )


# NOTE: min_value and max_value are BOTH INCLUSIVE
@st.composite
def triangle_valid_integers_tuple(draw):
    x = draw(st.integers(min_value=1))
    y = draw(st.integers(min_value=1))
    z = draw(st.integers(min_value=abs(x-y)+1, max_value=(x + y - 1)))
    return (x, y, z)


@given(triangle_valid_integers_tuple())
def test_valid_triangles_with_custom_strategy(triangle_tuple):
    assert Triangle(triangle_tuple).describe() in (
        'scalene', 'isosceles', 'equilateral'
    )


@given(triangle_valid_integers_tuple())
def test_arbitrary_scalenes_with_custom_strategy_assume(triangle_tuple):
    assume(
        triangle_tuple[0] != triangle_tuple[1] and
        triangle_tuple[1] != triangle_tuple[2] and
        triangle_tuple[0] != triangle_tuple[2]
    )
    assert Triangle(triangle_tuple).describe() == 'scalene'


@given(triangle_valid_integers_tuple().filter(
        lambda t:
        t[0] != t[1] and
        t[1] != t[2] and
        t[0] != t[2]
))
def test_arbitrary_scalenes_with_custom_strategy_filter(triangle_tuple):
    assert Triangle(triangle_tuple).describe() == 'scalene'


@st.composite
def triangle_isosceles_valid_integers_tuple(draw):
    x = draw(st.integers(min_value=1))
    y = x
    z = draw(st.integers(min_value=abs(x-y)+1, max_value=(x + y - 1)).filter(
        lambda z: z != x
    ))
    return (x, y, z)


@given(triangle_isosceles_valid_integers_tuple())
def test_arbitrary_isosceleses_with_custom_strategy(triangle_tuple):
    assert Triangle(triangle_tuple).describe() == 'isosceles'
