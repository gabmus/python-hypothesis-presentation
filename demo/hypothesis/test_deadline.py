from hypothesis import given, settings
from hypothesis.strategies import integers
from datetime import timedelta
from time import sleep


@given(integers())
@settings(deadline=timedelta(milliseconds=50), max_examples=1)
def test_sleep(x):
    sleep(1)  # sleeps for 1 second
    assert x == x
