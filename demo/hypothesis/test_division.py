from hypothesis import given
from hypothesis.strategies import integers


def division(x, y):
    return x//y


@given(integers(), integers().filter(lambda a: a != 0))
def test_division(x, y):
    z = x*y
    assert division(z, y) == x
