from src.my_queue import MyQueue
from hypothesis import strategies as st
from queue import SimpleQueue
from hypothesis.stateful import (
    rule,
    Bundle,
    precondition,
    invariant,
    RuleBasedStateMachine
)
from pytest import raises as assert_raises


class MyQueueMachine(RuleBasedStateMachine):
    def __init__(self):
        super(MyQueueMachine, self).__init__()
        self.sut = MyQueue()
        self.model = SimpleQueue()

    @invariant()
    def size_should_be_the_same(self):
        assert len(self.sut) == self.model.qsize()

    @rule(val=st.integers())
    def push_integer(self, val):
        self.sut.push(val)
        self.model.put(val)

    @rule(val=st.binary())
    def push_binary(self, val):
        self.sut.push(val)
        self.model.put(val)

    @rule()
    @precondition(lambda self: self.model.qsize() > 0)
    def take(self):
        assert self.sut.take() == self.model.get_nowait()

    @rule()
    @precondition(lambda self: self.model.qsize() == 0)
    def take_when_empty(self):
        with assert_raises(IndexError):
            self.sut.take()

    @rule()
    def reset(self):
        self.sut.clear()
        self.model = SimpleQueue()
        self.values = Bundle('values')


TestMyQueueMachine = MyQueueMachine.TestCase
