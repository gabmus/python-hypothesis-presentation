from src.sum import sum, buggy_sum
from hypothesis import given, settings, register_random
from hypothesis import strategies as st


@given(st.integers(), st.integers())
def test_any_sum(x, y):
    assert sum(x, y) == x+y


@given(st.integers(), st.integers())
@settings(max_examples=500)
def test_any_buggy_sum(x, y):
    assert buggy_sum(x, y) == x+y
