from src.sqrt import sqrt
from hypothesis import given  # , assume
from hypothesis import strategies as st


# limit max to 2**32 because based on how the function works
# it fails with higher numbers without changing MAX_ITERATIONS
@given(st.integers(min_value=1, max_value=2**32))
def test_sqrt_all_perfect_squares(n):
    assert sqrt(n*n) == n
