class MyQueue:
    def __init__(self):
        self.__list = []

    def clear(self):
        self.__list.clear()

    def push(self, x):
        self.__list.append(x)

    def take(self):
        return self.__list.pop(0)

    def __len__(self):
        return len(self.__list)

    def __getitem__(self, item):
        return self.__list[item]

    def __repr__(self):
        return self.__list.__repr__()

    def get_list(self):
        return self.__list.copy()
