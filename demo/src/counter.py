class BuggyCounter:
    def __init__(self, start=0):
        self.n = start
        self.count = 0
    
    def dec(self):
        self.n -= 1
        self.count += 1

    def inc(self):
        if self.count != 5:  # this is the bug
            self.n += 1
        self.count += 1

    def get(self):
        return self.n

    def reset(self):
        self.n = 0

    def double_it(self):
        self.n += self.n


class Counter:
    def __init__(self, start=0):
        self.n = start
        self.count = 0
    
    def dec(self):
        self.n -= 1
        self.count += 1

    def inc(self):
        self.n += 1
        self.count += 1

    def get(self):
        return self.n

    def reset(self):
        self.n = 0

    def double_it(self):
        self.n += self.n
