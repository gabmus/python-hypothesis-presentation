from typing import Tuple
from functools import reduce
from operator import and_, or_


class Triangle:
    def __init__(self, sides: Tuple[int, int, int]):
        assert reduce(and_, [side > 0 for side in sides]), \
                'Sides must be positive'
        assert reduce(and_, [
            sides[-i] < (sides[1-i] + sides[2-i]) for i in range(3)
        ]), 'One side cannot be bigger than the sum of the other two'
        self.sides = sides

    def describe(self) -> str:
        return 'equilateral' if reduce(
            and_,
            [self.sides[1-i] == self.sides[-i] for i in range(2)]
        ) else (
            'isosceles' if reduce(
                or_,
                [self.sides[-i] == self.sides[1-i] for i in range(3)]
            ) else 'scalene'
        )
