from random import randint


def sum(x, y):
    return x+y


def buggy_sum(x, y):
    if randint(0, 10) == 5:
        x += 1
    return x+y
