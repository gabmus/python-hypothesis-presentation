MAX_ITERATIONS = 10000


def error(guess, square):
    return abs(guess*guess - square)


def sqrt(x):
    guess = 1
    count = 0
    while error(guess, x) > 1:
        q = x/guess
        guess = (guess+q) / 2
        count += 1
        if count > MAX_ITERATIONS:
            break
    delta = error(guess, x)
    if error(guess-1, x) < delta:
        return int(guess)-1
    if error(guess+1, x) < delta:
        return int(guess)+1
    return int(guess)
